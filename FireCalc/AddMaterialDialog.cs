using System;
using System.Collections.Generic;
using FireCalc.StaticData;

namespace FireCalc
{
	public partial class AddMaterialDialog : Gtk.Dialog
	{
		private List<MaterialData> _data;

		public AddMaterialDialog (List<MaterialData> data)
		{
			_data = data;
			this.Build ();
		}

		protected void OnButtonOkClicked (object sender, EventArgs e)
		{
		}
	}
}

