using System;
using System.Collections.Generic;

namespace FireCalc.StaticData
{
	public class EmissionData
	{
		public EmissionData () {}
	}

	/// <summary>
	/// Material data.
	/// </summary>
	public abstract class MaterialData
	{
		/// <summary>
		/// Gets or set the mass.
		/// </summary>
		/// <value>The mass.</value>
		public abstract double Mass { get; set; }

		/// <summary>
		/// Gets the low level combustion.
		/// </summary>
		/// <value>The low level combustion.</value>
		public abstract double LowLevelCombustion { get; }

		/// <summary>
		/// Gets the air number.
		/// </summary>
		/// <value>The air number.</value>
		public abstract double AirNumber { get; }

		/// <summary>
		/// Gets the rate of prol.
		/// </summary>
		/// <value>The rate of prol.</value>
		public abstract double RateOfProl { get; }

		/// <summary>
		/// Gets the emissions.
		/// </summary>
		/// <value>The emissions.</value>
		public abstract Dictionary<EmissionData, double> Emissions { get; }
	}

	public class Wood : MaterialData
	{
		public override double Mass {
			get {
				return this.Mass;
			}

			set {
				this.Mass = value;
			}
		}

		public override double LowLevelCombustion {
			get {
				return 13.8;
			}
		}

		public override double AirNumber {
			get {
				return 4.2;
			}
		}

		public override double RateOfProl {
			get {
				throw new NotImplementedException ();
			}
		}

		public override Dictionary<EmissionData, double> Emissions {
			get {
				throw new NotImplementedException ();
			}
		}
	}
}

