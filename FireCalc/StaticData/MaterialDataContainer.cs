using System;
using System.Collections.Generic;

namespace FireCalc.StaticData
{
	/// <summary>
	/// Material data.
	/// Single tone class.
	/// </summary>
	public class MaterialDataContainer
	{
		/// <summary>
		/// The instance.
		/// </summary>
		private static MaterialDataContainer instance;
		private MaterialDataContainer() {}
		private List<MaterialData> data = new List<MaterialData>();

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <value>The instance.</value>
		public static MaterialDataContainer Instance
		{
			get {
				if (instance == null) 
					instance = new MaterialDataContainer();

				return instance;
			}
		}

		public void AddMaterial(MaterialData data)
		{
			instance.data.Add (data);
		}

		/// <summary>
		/// Sets the data.
		/// </summary>
		/// <returns>The data container.</returns>
		/// <param name="data">List MaterialData.</param>
		public MaterialDataContainer SetData(List<MaterialData> data)
		{
			instance.data = data;
			return instance;
		}

		/// <summary>
		/// Find the specified predicate.
		/// </summary>
		/// <param name="predicate">Predicate.</param>
		public MaterialData Find(Predicate<MaterialData> predicate)
		{
			return data.Find (predicate);
		}
	}
}