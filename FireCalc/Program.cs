using System;
using Gtk;
using FireCalc.StaticData;

namespace FireCalc
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();
			MainWindow win = new MainWindow ();
			win.Show ();

			fillData ();

			Application.Run ();
		}

		private static void fillData()
		{
			var container = MaterialDataContainer.Instance;
			container.AddMaterial (new Wood ());
		}
	}
}
