using System;
using System.Linq;
using Gtk;
using FireCalc.StaticData;

namespace FireCalc.Calculate
{
	public static class Extension
	{
		public static string f(this string self, params object []args)
		{
			return string.Format (self, args);
		}
	}

	public static class Calculate
	{
		private static dynamic _data;
		private static TextView _log;

		public static void Init(dynamic data, TextView log)
		{
			var wood = new Wood ();

			_data = data;
			_log = log;

			log.Buffer.Clear ();

			double square = (double)(data.width * data.length);
			log.Buffer.Text += "Square = {0}\n".f (square);

			double volume = (double)(data.width * data.height * data.length);
			log.Buffer.Text += "Volume = {0}\n".f (volume);
			
			double opening = (data.sOpen * Math.Sqrt (data.hOpen)) / square;
			log.Buffer.Text += "Opening = {0}\n".f (opening);

			double q = (double)(data.materialMass / square);
			log.Buffer.Text += "q = {0}\n".f (q);

			double v = (double)((wood.AirNumber * data.materialMass) / data.materialMass);
			log.Buffer.Text += "V = {0}\n".f (v);

			double qk = (double)((data.materialMass * wood.LowLevelCombustion)/((6*(Math.Pow(volume, 0.667)) - data.sOpen)*wood.LowLevelCombustion));
			log.Buffer.Text += "qk = {0}\n".f (qk);

			double qkp = (4500.0*Math.Pow(opening, 3)) / (1.0 + 500.0*Math.Pow(opening, 3)) + Math.Pow (volume, 0.333) / (6 * v);
			log.Buffer.Text += "qkp = {0}\n".f (qkp);

			double Tmax = 940 * Math.Exp ((4.7 * Math.Pow (10, - 3)) * (q - 30));
			log.Buffer.Text += "Tmax = {0}\n".f (Tmax);
		}
	}
}

