using System;
using Gtk;
using System.Linq;
using FireCalc.Calculate;
using System.Collections.Generic;
using FireCalc.StaticData;

public partial class MainWindow: Gtk.Window
{	
	public List<MaterialData> MaterialData = new List<MaterialData>();

	public MainWindow (): base (Gtk.WindowType.Toplevel)
	{
		Build ();
	}

	protected void OnDeleteEvent (object sender, DeleteEventArgs a)
	{
		Application.Quit ();
		a.RetVal = true;
	}

	protected void OnButton2Clicked (object sender, EventArgs e)
	{
		var dialog = new FireCalc.AddMaterialDialog (MaterialData);
		dialog.Parent = this;
		dialog.Show ();
	}

	protected void OnButton108Clicked (object sender, EventArgs e)
	{
		notebook1.Page = 0;
		var log = (TextView)this.GtkScrolledWindow.Children.First (x => x.Name == "calculateLog");

		var data = new {
			length = this.length.Value,
			width = this.width.Value,
			height = this.height.Value,
			sOpen = this.squareOpen.Value,
			hOpen = this.heightOpen.Value,
			isInCity = this.isInCity.Active,
			materialMass = this.materialMass.Value
		};

		Calculate.Init (data, log);
	}
}
